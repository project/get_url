# Twig Get URL

The "Twig Get URL" module for Drupal provides functionality to generate URLs directly within Twig templates using a node's ID. This simplifies the process of creating links in your templates and ensures that they adhere to Drupal's routing system.


For a full description of the module, visit the
[project page](https://www.drupal.org/project/get_url).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/get_url).


## Table of contents

- Requirements
- Installation
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Maintainers

- Seamus Campbell - [scampbell1](https://www.drupal.org/u/scampbell1)