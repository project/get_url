<?php

namespace Drupal\get_url;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Provides a Twig extension for generating node URLs.
 */
class TwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   *
   * This function must return the name of the extension. It must be unique.
   */
  public function getName() {
    return 'get_node_url';
  }

  /**
   * Returns an array of Twig functions.
   *
   * In this function we can declare the extension function.
   */
  public function getFunctions() {
    return [
      new TwigFunction('get_url', $this->getUrl(...), ['is_safe' => ['html']]),
    ];
  }

  /**
   * Returns the URL alias for a given node ID.
   *
   * @param string $nodeId
   *   The node ID.
   *
   * @return string
   *   The URL alias.
   */
  public static function getUrl($nodeId) {
    $alias = \Drupal::service('path_alias.manager')->getAliasByPath($nodeId);
    return $alias;
  }

}
